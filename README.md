#Setup instructions
1. set up weaver using the config file `/weaver.yaml`
2. For authorship:
    1. load GraphML file
    2. call the node programs, e.g., in Python,

            >> from weaver import client
            >> c = client.Client(‘128.84.167.207’,2002)
            >> c.discover_paths(“Yong Yu”, “Jie Zhang”, path_len=4)))

3. For N-grams:
    1. Get the raw data of the papers, put it under `/data`
    2. Run `python parser_for_ngram_bulk.py` to generate GraphML file
    3. load this GraphML file
