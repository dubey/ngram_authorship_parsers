f = open("citeseerx_alldata.xml","r")
g = open("sample.xml","w")

line = f.readline()
g.write(line)
g.write("<all_data>\n")

i = 0

while True:
    line = f.readline()
    i = i + 1
    if line == '':
        break
    if not line.startswith("<?xml"):
        g.write(line)
	if i > 1000000 and line == "</OAI-PMH>\n":
		break

g.write("</all_data>\n")
f.close()
g.close()
