from sets import Set
from parser_utils import *
from utils import convert_str_to_date
import codecs
import time
import re
import json
import os

include_abs  = False
in_filename  = '/home/dubey/datasets/citeseer_data/data.xml'
if include_abs:
    out_filename = '/home/dubey/datasets/citeseer_data/docs.json'
else:
    out_filename = '/home/dubey/datasets/citeseer_data/docs_no_abstracts.json'

with codecs.open(in_filename, encoding='utf-8') as data_f, open(out_filename, 'w') as out:
    data_file_size = os.path.getsize(in_filename)
    last_progress = 0
    last_time     = time.time()
    first_time    = last_time
    article_id = 0
    title      = ''
    abstract   = ''
    url        = ''
    date       = None
    for line in data_f:
        l = preprocess_line(line)
        if check_xml_tag(l, 'dc:title'):
            title = check(get_xml_data(l))
        elif check_xml_tag(l, 'dc:description') and include_abs:
            abstract = check(get_xml_data(l))
        elif check_xml_tag(l, 'dc:date'):
            date_data = get_xml_data(l)
            cur_date  = None
            if date_data is not None:
                try:
                    cur_date = convert_str_to_date(check(date_data))
                    if date is None or cur_date < date:
                        date = cur_date
                except:
                    pass
        elif check_xml_tag(l, 'dc:identifier'):
            url = check(get_xml_data(l))
            if legal_title(title) and date is not None:
                if include_abs and len(abstract) > 0:
                    article_id += 1
                    json.dump({'id':       article_id,
                               'title':    title,
                               'date':     str(date),
                               'url':      url,
                               'abstract': abstract}, out)
                    out.write('\n')
                elif not include_abs:
                    article_id += 1
                    json.dump({'id':       article_id,
                               'title':    title,
                               'date':     str(date),
                               'url':      url}, out)
                    out.write('\n')
            title    = ''
            abstract = ''
            url      = ''
            date     = None

        progress = float(data_f.tell())/data_file_size
        if progress > last_progress + 0.01:
            cur_time      = time.time()
            print 'progress=%f cur_time=%f total_time=%f' % (progress, cur_time - last_time, cur_time - first_time)
            last_progress = progress
            last_time     = cur_time
