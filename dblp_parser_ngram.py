from sets import Set
from parser_utils import *
from utils import convert_str_to_date
import codecs
import time
import re
import sys

article_id = 0
doc_map   = {}

filter_list = set(['a', 'an', 'the', 'this', 'that', 'these', 'those', 'in', 'on',
    'at', 'under', 'above', 'below', 'for', 'be', 'have', 'it', 'with', 'as',
    'to', 'of', 'by', 'then', 'is', 'are', 'am', 'be', 'when', 'what', 'why',
    'how', 'where', 'and', 'or', 'not', 'no', 'yes'])

def process_paper(article_id, title, date):
    global doc_map
    doc_map[title.lower()] = date

with codecs.open(data_filename, encoding='utf-8') as data_f:
    last_progress = 0
    last_time     = time.time()
    first_time    = last_time
    title         = ''
    date          = 0
    for line in data_f:
        l = preprocess_line(line)
        if check_xml_tag(l, 'article'):
            title    = ''
            date     = 0
        elif check_xml_tag(l, 'title'):
            dblp_title = check(get_xml_data(l))
            title = dblp_title[:-1] if len(dblp_title) > 0 and dblp_title[-1] == '.' else dblp_title
        elif check_xml_tag(l, 'year'):
            date = int(get_xml_data(l))
        elif check_xml_tag(l, '/article'):
            if legal_title(title) and date != 0:
                article_id += 1
                process_paper(article_id, title, date)

        progress = float(data_f.tell())/data_file_size
        if progress > last_progress + 0.01:
            cur_time      = time.time()
            print 'progress=%f cur_time=%f total_time=%f' % (progress, cur_time - last_time, cur_time - first_time)
            last_progress = progress
            last_time     = cur_time

print 'Done pass, #docs=%d' % article_id
