from sets import Set
from parser_utils import *
import utils
import time
import re
import sys
import codecs
import json

assert len(sys.argv) == 2

graph_idx = int(sys.argv[1])

article_id = 0
dblp_map   = {}
edge_id    = 0
word_map   = {}
dblp_file          = '/mnt/hdd/dblp_data/dblp.xml'
dblp_json_file     = '/mnt/hdd/dblp_data/docs.json'
citeseer_file      = '/home/dubey/datasets/citeseer_data/data.xml'
citeseer_json_file = '/home/dubey/datasets/citeseer_data/docs.json'

node_f = open('/home/dubey/datasets/dated_ngram/nodes_%d.xml' % graph_idx, 'w')
edge_f = open('/home/dubey/datasets/dated_ngram/edges_%d.xml' % graph_idx, 'w')

#filter_list = set()
filter_list = set(['a', 'an', 'the', 'this', 'that', 'these', 'those', 'in', 'on',
    'at', 'under', 'above', 'below', 'for', 'be', 'have', 'it', 'with', 'as',
    'to', 'of', 'by', 'then', 'is', 'are', 'am', 'be', 'when', 'what', 'why',
    'how', 'where', 'and', 'or', 'not', 'no', 'yes'])

def valid_word(word):
    return (len(word) > 0) and (word not in filter_list) and (not has_digit(word))

def my_word(word):
    #return True
    global graph_idx
    char = ord(word[0])
    cur_idx = 9
    if   char >= 97 and char <= 122:
        cur_idx = (char - 97)/3
    elif char >= 65 and char <= 90:
        cur_idx = (char - 65)/3
    return cur_idx == graph_idx

def add_word(word):
    if my_word(word) and word not in word_map:
        node_f.write("<node id=\"%s\"></node>\n" % word)
        word_map[word] = {}

def add_edge(word1, word2, aid, pos, year):
    if my_word(word1):
        wmap = word_map[word1]
        if word2 not in wmap:
            wmap[word2] = {aid: (year, [pos])}
        else:
            w2map = wmap[word2]
            if aid not in w2map:
                w2map[aid] = (year, [pos])
            else:
                w2map[aid][1].append(pos)

def process_abstract(abstract, article_id, title, year, url):
    lst        = re.findall('\w+', abstract.lower())
    last       = ''
    pos        = 0
    valid_last = False
    for word in lst:
        valid_cur = valid_word(word)
        if valid_cur:
            add_word(word)
            if valid_last:
                add_edge(last, word, article_id, pos, year)
        last       = word
        pos       += 1
        valid_last = valid_cur
    if valid_last:
        add_edge(last, 'a', article_id, -1, year) # the last word need a outdegree

add_word('a')

def make_title_key(title):
    list_title = list(title)
    for i in range(len(list_title)):
        if title[i] == '-' or title[i] == ':' or title[i] == '\\':
            list_title[i] = ' '
    return ''.join(list_title)

def process_dblp_paper(title, year):
    global dblp_map
    title_key = make_title_key(title.lower())
    dblp_map[title_key] = year

#print 'starting DBLP pass'
#with open(dblp_json_file, 'r') as data_f:
#    data_file_size = os.path.getsize(dblp_json_file)
#    last_progress  = 0
#    last_time      = time.time()
#    first_time     = last_time
#    title          = ''
#    year           = 0
#    for line in data_f:
#        l = preprocess_line(line)
#        doc = json.loads(l)
#        process_dblp_paper(doc['title'], doc['year'])
#
#        progress = float(data_f.tell())/data_file_size
#        if progress > last_progress + 0.01:
#            cur_time      = time.time()
#            print 'progress=%f cur_time=%f total_time=%f' % (progress, cur_time - last_time, cur_time - first_time)
#            last_progress = progress
#            last_time     = cur_time

print 'done DBLP pass, starting CiteSeer pass'

excluded      = 0
with open(citeseer_json_file, 'r') as data_f:
    data_file_size = os.path.getsize(citeseer_json_file)
    last_progress  = 0
    last_time      = time.time()
    first_time     = last_time
    for line in data_f:
        l = preprocess_line(line)
        doc = json.loads(l)
        title_key = make_title_key(doc['title'].lower())
        #if title_key in dblp_map:
        #    year = dblp_map[title_key]
        year        = utils.convert_str_to_date(doc['date']).year
        article_id += 1
        process_abstract(doc['abstract'],
                         article_id,
                         doc['title'],
                         year,
                         doc['url'])
        doc['id']   = article_id
        doc['date'] = year
        #else:
        #    excluded += 1

        progress = float(data_f.tell())/data_file_size
        if progress > last_progress + 0.01:
            cur_time      = time.time()
            print 'progress=%f cur_time=%f total_time=%f' % (progress, cur_time - last_time, cur_time - first_time)
            last_progress = progress
            last_time     = cur_time

print 'Done citeseer pass, #nodes=%d #docs=%d #excluded=%d' % (len(word_map), article_id, excluded)

for word1 in word_map:
    for word2 in word_map[word1]:
        for article in word_map[word1][word2]:
            edge_id += 1
            year, pos_list = word_map[word1][word2][article]
            assert len(pos_list) > 0
            positions = str(pos_list[0])
            for i in range(1, len(pos_list)):
                positions += ',' + str(pos_list[i])
            edge_f.write('<edge id=\"%d\" source=\"%s\" target=\"%s\"><data key=\"doc\">%d</data><data key=\"pos\">%s</data><data key=\"date\">%d</data></edge>\n'
                % (edge_id, word1, word2, article, positions, year))


node_f.close()
edge_f.close()
