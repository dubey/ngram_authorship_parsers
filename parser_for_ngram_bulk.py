from sets import Set
from parser_utils import *
from utils import convert_str_to_date
import codecs
import time
import re
import sys

assert len(sys.argv) == 2

graph_idx = int(sys.argv[1])

node_f = open('/mnt/hdd/ngram_graph/nodes_%d.xml' % graph_idx, 'w')
edge_f = open('/mnt/hdd/ngram_graph/edges_%d.xml' % graph_idx, 'w')
article_id = 0
edge_id    = 0
word_map   = {}
num_docs   = 0

filter_list = set(['a', 'an', 'the', 'this', 'that', 'these', 'those', 'in', 'on',
    'at', 'under', 'above', 'below', 'for', 'be', 'have', 'it', 'with', 'as',
    'to', 'of', 'by', 'then', 'is', 'are', 'am', 'be', 'when', 'what', 'why',
    'how', 'where', 'and', 'or', 'not', 'no', 'yes'])

def valid_word(word):
    return (len(word) > 1) and (word not in filter_list) and (not has_digit(word))

def my_word(word):
    global graph_idx
    char = ord(word[0])
    cur_idx = 9
    if   char >= 97 and char <= 122:
        cur_idx = (char - 97)/3
    elif char >= 65 and char <= 90:
        cur_idx = (char - 65)/3
    return cur_idx == graph_idx

def add_document(doc_id, doc_title, doc_date, doc_url):
    global num_docs
    num_docs += 1
    node_f.write('<node id=\"d:%d\"><data key=\"title\">%s</data><data key=\"date\">%s</data><data key=\"url\">%s</data></node>\n'
                    % (doc_id, doc_title, str(doc_date), doc_url))

def add_word(word):
    if my_word(word) and word not in word_map:
        node_f.write("<node id=\"%s\"></node>\n" % word)
        word_map[word] = {}

def add_edge(word1, word2, aid, pos, date):
    if my_word(word1):
        wmap = word_map[word1]
        if word2 not in wmap:
            wmap[word2] = {aid: (date, [pos])}
        else:
            w2map = wmap[word2]
            if aid not in w2map:
                w2map[aid] = (date, [pos])
            else:
                w2map[aid][1].append(pos)

def process_abstract(abstract, article_id, title, date, url):
    lst        = re.findall('\w+', abstract.lower())
    last       = ''
    pos        = 0
    valid_last = False
    #add_document(article_id, title, date, url)
    for word in lst:
        valid_cur = valid_word(word)
        if valid_cur:
            add_word(word)
            if valid_last:
                add_edge(last, word, article_id, pos, date)
        last       = word
        pos       += 1
        valid_last = valid_cur
    if valid_last:
        add_edge(last, 'a', article_id, -1, date) # the last word need a outdegree

add_word('a')

with codecs.open(data_filename, encoding='utf-8') as data_f:
    last_progress = 0
    last_time     = time.time()
    first_time    = last_time
    title         = ''
    abstract      = ''
    url           = ''
    date          = None
    for line in data_f:
        l = preprocess_line(line)
        if check_xml_tag(l, 'dc:title'):
            title = check(get_xml_data(l))
        elif check_xml_tag(l, 'dc:description'):
            abstract = check(get_xml_data(l))
        elif check_xml_tag(l, 'dc:date'):
            date_data = get_xml_data(l)
            cur_date  = None
            if date_data is not None:
                try:
                    cur_date = convert_str_to_date(check(date_data))
                    if date is None or cur_date < date:
                        date = cur_date
                except:
                    pass
        elif check_xml_tag(l, 'dc:identifier'):
            url = check(get_xml_data(l))
            if legal_title(title):
                article_id += 1
            if len(abstract) > 0 and date is not None:
                process_abstract(abstract, article_id, title, date, url)
            title    = ''
            abstract = ''
            url      = ''
            date     = None

        progress = float(data_f.tell())/data_file_size
        if progress > last_progress + 0.01:
            cur_time      = time.time()
            print 'progress=%f cur_time=%f total_time=%f' % (progress, cur_time - last_time, cur_time - first_time)
            last_progress = progress
            last_time     = cur_time

print 'Done first pass, #nodes=%d #docs=%d' % (len(word_map), num_docs)

for word1 in word_map:
    for word2 in word_map[word1]:
        for article in word_map[word1][word2]:
            edge_id += 1
            date, pos_list = word_map[word1][word2][article]
            assert len(pos_list) > 0
            positions = str(pos_list[0])
            for i in range(1, len(pos_list)):
                positions += ',' + str(pos_list[i])
            edge_f.write('<edge id=\"%d\" source=\"%s\" target=\"%s\"><data key=\"doc\">%d</data><data key=\"pos\">%s</data><data key=\"date\">%s</data></edge>\n'
                % (edge_id, word1, word2, article, positions, str(date)))


node_f.close()
edge_f.close()
