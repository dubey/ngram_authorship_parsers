from sets import Set
from parser_utils import *
import utils
import time
import re
import sys
import codecs
import json
import string

PAIR_FLUSH_COUNT = 10000
article_id = 0
edge_id    = 0
word_set   = set()
word_map   = {}
citeseer_file      = '/home/dubey/datasets/citeseer_data/data.xml'
citeseer_json_file = '/home/dubey/datasets/citeseer_data/docs.json'
nodes_file         = '/mnt/hdd/consolidated_ngram_graph/nodes.xml'
edges_file         = '/mnt/hdd/consolidated_ngram_graph/edges.xml'

filter_list = set(['a', 'an', 'the', 'this', 'that', 'these', 'those', 'in', 'on',
    'at', 'under', 'above', 'below', 'for', 'be', 'have', 'it', 'with', 'as',
    'to', 'of', 'by', 'then', 'is', 'are', 'am', 'be', 'when', 'what', 'why',
    'how', 'where', 'and', 'or', 'not', 'no', 'yes'])

def valid_word(word):
    return (len(word) > 0) and (not has_digit(word))

def add_word(word, graph_f):
    global word_set
    global word_map
    if word not in word_set:
        graph_f.write("<node id=\"%s\"></node>\n" % word)
        word_set.add(word)
    if word not in word_map:
        word_map[word] = {}

def add_edge(word1, word2, doc, date, pos):
    global word_map
    global PAIR_FLUSH_COUNT
    wmap  = word_map[word1]
    if word2 not in wmap:
        wmap[word2] = [{}, 0]
    w2pair = wmap[word2]
    w2map  = w2pair[0]
    if doc not in w2map:
        w2map[doc] = ([pos], date)
    else:
        w2map[doc][0].append(pos)
    w2pair[1] += 1
    return w2pair[1] > PAIR_FLUSH_COUNT

def flush_edge(word1, word2, graph_f):
    global word_map
    global edge_id
    wmap = word_map[word1]
    assert word2 in wmap
    w2pair = wmap[word2]
    w2map  = w2pair[0]
    prop_val = ''
    for doc in w2map:
        pos_list, date = w2map[doc]
        prop_val += str((doc, date, pos_list)) + ';'
    del wmap[word2]

    edge_id += 1
    prop_val = prop_val[:-1]
    graph_f.write('<edge id=\"%d\" source=\"%s\" target=\"%s\"><data key=\"locs\">%s</data></edge>\n' % (edge_id, word1, word2, prop_val))

def process_abstract(abstract, article_id, title, date, url, node_f, edge_f):
    lst        = re.findall('\w+', abstract.lower())
    last       = ''
    pos        = 0
    valid_last = False
    lst_len    = len(lst)
    lst.append('$')
    flush_pair = set([])
    for i in range(lst_len):
        if lst[i] in filter_list:
            lst[i] = '%s_%s' % (lst[i], lst[i+1][0])
    for i in range(lst_len):
        add_word(lst[i], node_f)
        if add_edge(lst[i], lst[i+1], article_id, date, pos):
            if (lst[i], lst[i+1]) not in flush_pair:
                flush_pair.add((lst[i], lst[i+1]))
        pos += 1
    for fp in flush_pair:
        flush_edge(fp[0], fp[1], edge_f)

print 'starting CiteSeer pass'

with open(citeseer_json_file, 'r') as data_f, open(nodes_file, 'w') as node_f, open(edges_file, 'w') as edge_f:
    add_word('$', node_f)
    data_file_size = os.path.getsize(citeseer_json_file)
    last_progress  = 0
    last_time      = time.time()
    first_time     = last_time
    for line in data_f:
        l = preprocess_line(line)
        doc = json.loads(l)
        year        = utils.convert_str_to_date(doc['date']).year
        article_id += 1
        process_abstract(doc['abstract'],
                         article_id,
                         doc['title'],
                         year,
                         doc['url'],
                         node_f, edge_f)
        doc['id']   = article_id
        doc['date'] = year

        progress = float(data_f.tell())/data_file_size
        if progress > last_progress + 0.01:
            cur_time      = time.time()
            print 'progress=%f cur_time=%f total_time=%f' % (progress, cur_time - last_time, cur_time - first_time)
            last_progress = progress
            last_time     = cur_time
        #if progress > 0.05:
        #    break

print 'Done citeseer pass, #nodes=%d #edges=%d #docs=%d' % (len(word_set), edge_id, article_id)
