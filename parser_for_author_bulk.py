from parser_utils import *
import time
import codecs

node_map = {}
out = open('/local/dubey/author_graph/sample.xml', 'w')
lst = []
article_id = 0
author_id = 0
edge_id = 0

def addNode(name):
    global author_id
    if name not in node_map:
        author_id = author_id + 1
        node_map[name] = {}
        out.write('<node id=\"%s\"></node>\n' % (name))

def record_edge(a1, a2):
    if a2 in node_map[a1]:
        node_map[a1][a2] += 1
    else:
        node_map[a1][a2]  = 1

def addEdge(a1, a2):
    global article_id
    addNode(a1)
    addNode(a2)
    record_edge(a1, a2)
    record_edge(a2, a1)

def add_authors(aid, title):
    for name1 in lst:
        for name2 in lst:
            if name1 != name2:
                e = addEdge(name1, name2)

with codecs.open(data_filename, encoding='utf-8') as data_f:
    last_progress = 0
    last_time     = time.time()
    first_time    = last_time
    for line in data_f:
        processed = preprocess_line(line)
        if check_xml_tag(processed, 'dc:title'):
            title = check(get_xml_data(processed))
        elif check_xml_tag(processed, 'dc:creator'):
            name  = check(get_xml_data(processed))
            if name in lst or len(name) < 1:
                continue
            lst.append(name)
        elif check_xml_tag('dc:identifier'):
            if legal_title(title):
                article_id += 1
                add_authors(article_id, title)
            lst  = []
            title = ''

        progress = float(data_f.tell())/data_file_size
        if progress > last_progress + 0.01:
            cur_time      = time.time()
            print 'progress=%f cur_time=%f total_time=%f' % (progress, cur_time - last_time, cur_time - first_time)
            last_progress = progress
            last_time     = cur_time

print 'Done first pass, #nodes=%d' % len(node_map)

for author in node_map:
    for nbr in node_map[author]:
        edge_id += 1
        out.write('<edge id=\"%d\" source=\"%s\" target=\"%s\"><data key=\"numdocs\">%d</data></edge>\n'
            % (edge_id, author, nbr, node_map[author][nbr]))

out.close()
