from datetime import date
from datetime import timedelta
from numpy import histogram
import json
from sys import maxsize
from load_doc_info import find_doc
import re

std_day = date(2000,1,1)

def convert_str_to_date(string):
    mm = dd = 1
    match = re.match("(\d{4})-(\d{1,2})-(\d{1,2})", string)
    if match:
        dd = int(match.group(3))
    else:
        match = re.match("(\d{4})-(\d{1,2})", string)
    if match:
        mm = int(match.group(2))
    else:
        match = re.match("(\d{4})", string)
    if match:
        yy = int(match.group(1))
    else:
        raise Exception(string)
    return date(yy,mm,dd)

def date_to_int(string):
    global std_day
    return (convert_str_to_date(string) - std_day).days

def date_between(now, start, end):
    numnow = date_to_int(now)
    return date_to_int(start) <= numnow and numnow < date_to_int(end)
