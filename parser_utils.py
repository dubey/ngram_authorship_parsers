from sets import Set
import os.path
import re

#data_filename  = "/mnt/hdd/citeseer_data/data.xml"
#data_file_size = os.path.getsize(data_filename)

def check(str):
    #return str.replace('$',' ').strip().encode('utf-8')
    return str.strip()

def legal_title(string):
    return len(re.findall('\w+', string))>1

def get_xml_data(text):
    return text.split('>')[1].split('<')[0]

def preprocess_line(text):
    return text.strip().encode('utf-8')

def check_xml_tag(text, tag):
    return text.startswith('<%s>' % tag)

def has_digit(text):
    return any(char.isdigit() for char in text)
